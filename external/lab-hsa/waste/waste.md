---
layout: page
permalink: /external/lab-hsa/waste/
shortcut: lab:waste
redirect_from:
  - /cards/lab-hsa:waste
  - /external/cards/lab-hsa:waste
  - /lab/waste
  - /external/external/lab-hsa/waste/
  - /cards/lab:waste
  - /external/cards/lab:waste
---
# Chemical and Biological Waste Management

To ensure a clean and safe lab environment, it is essential to know how to discard the waste in a proper way. 
Please follow the [Docebo Training](https://cdn5.dcbstatic.com/dcd/scormapi_v60/launcher_full.html?host=unisupport.docebosaas.com&id_user=13109&id_reference=297&scorm_version=1.2&id_resource=145&id_item=145&idscorm_organization=145&id_package=145&launch_type=fullscreen&id_course=42&player=hydra&autoplay_enabled=0&name=waste-management-concept&return_url=unisupport.docebosaas.com&as_json=1&auth_code=11a2729cf9428533597f378018d08c615064b379&use_sw=1&context=lms&rtl=false) and refer to the **Chemical and Biological Waste Management concept of the University**:

<div align="center">
<img src="img/1.png">
</div>

For any question regarding safety and waste management, please send a [ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=f140d745dbe83c905c72ef3c0b96194e&sysparm_category=cca7f2c1db683c905c72ef3c0b961940) to the LCSB lab safety team. 
