---
layout: page
permalink: /external/lab-hsa/spill/
shortcut: lab:spill
redirect_from:
  - /cards/lab-hsa:spill
  - /external/cards/lab-hsa:spill
  - /lab/spill
  - /external/external/lab-hsa/spill/
  - /cards/lab:spill
  - /external/cards/lab:spill
---

# Spill in a laboratory

Chemical or biological spills may happen. First of all stay calm, each situation is different, but following the below instructions will enable you to face any situation.

During the emergency phase (first phase) you have to ensure your protection and your colleagues' protection.
Prevent aggravation if possible (recap flasks, shut down power or heat source).

In case somebody has been exposed by contact to chemicals (skin or mucosa) rinse with **Diphoterine** as soon as possible.
To know more about the usage of Diphoterine, consult the dedicated card <a href="{{ '/lab-hsa/diphoterine/' | relative_url }}">here</a>.

Look for assitance to evacuate the laboratories and to take care of any injured people.
Leave the area and prepare the second phase, the spill management.

Contact the **5555, always!** People from the 5555 will come to help you and will contact the safety officer. 
During working hours you can also contact the safety officer to save time. 
In case of injured people you are allowed to contact the 112 before contacting the 5555. 
To be sure the labs are evacuated, the lab technicians and the room responsible can also help you.

Additionnaly, in case of any contamination with chemical compound (by contact, inhalation, ingestion) check the section 4 of the SDS of the product, refering to the first aids measures. 

If a victim is sent to the hospital, he should take a copy of the SDS to be provided to the medical staff. In case of contact with biological reagents, rinse with water for 15min. You can use the eye shower of the lab/corridor.


## First phase: emergency measures in summary

The first thing to do is to follow the **SPILL** sequence:

<div align="center">
<img src="img/img1.png" width="500">
</div>

You can find the signalisation material needed to lock the aera and the spill kit to manage the spill at the safety points.

<div align="center">
<img src="img/img2.png" width="700">
</div>

When you are done with the emergency measures, you can start the **spill management**. 

## Second phase: the spill management

The laboratory evacuated during the first phase, the second phase can start, with the planning of the intervention and the start of a 30 min delay to allow aerosols to settle down.

**Only wetlab people who has followed the spill training (theory + practical session) can manage the spill.**

- Wait for **30min** before entering again the laboratory. During this time, prepare the spill intervention, outside of the laboratory. 
- It is mandatory to be **2 persons** to manage the spill, never act alone. If you are alone in the lab, call the 5555, they will help you in the spill management. 
- If you don't feel confident, do not start the spill management, **ask for help** to an experienced colleague or the safety officer. 
- Prepare the **waste containers** and the **personal protective equipment**.  
- Think how to remove the PPE without contaminating yourself. 
- In any case, **report** the incident afterwards for follow up.

You will find here below illustrations of interventions for chemical and biological spills.

### Chemical Spill

1. Wait 30min between the spill and the intervention. 
2. Wear the appropriate PPE (refer to the safety datasheet).
3. Plan your work and your sequence of operations to avoid contaminating yourself.
4. Contain the spill with Trivorex
5. Sprinkle to cover the entire spill and wait for 10 min
6. Remove with the shovel from the spill kit and discard it in a plastic bag that will be discarded in the blue bin with yellow cover (solids and liquids contaminated with biologicals and chemicals)

<div align="center">
<img src="img/img3.png">
</div>

### Biological Spill

1. Wait 30min between the spill and the intervention. This time is allowing all the potentially generated aerosols to fall down.
2. Wear the appropriate PPE.
3. Plan your work and your sequence of operations to avoid contaminating yourself.
4. Cover the spill with absorbant paper

<div align="center">
<img src="img/img7.png" width="700">
</div>

5. Disinfect. The aerea of disinfection should be twice as big as the size of the spill.
    - For small spill (~10mL), use the disinfectant from the lab
    - For larger spill, use the disinfectant from the spill kit

<div align="center">
<img src="img/img5.png" width="700">
</div>

6. Respect the contact time of 10min for disinfection
7. Collect the absorbant paper and discard it in the blue bin with yellow cover (solids and liquids contaminated with biologicals and chemicals)

<div align="center">
<img src="img/img6.png" width="700">
</div>
