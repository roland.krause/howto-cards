---
layout: page
permalink: /external/lab-hsa/ppe/
shortcut: lab:ppe
redirect_from:
  - /cards/lab-hsa:ppe
  - /external/cards/lab-hsa:ppe
  - /lab/ppe
  - /external/external/lab-hsa/ppe/
  - /cards/lab:ppe
  - /external/cards/lab:ppe
---

# Personal Protective Equipment (PPE)

To protect every person present in the laboratories from any contamination or exposure to hazardous materials, chemical or biological, each person entering the laboratories has to wear the appropriate Personal Protective Equipment (PPE).

Every LCSB employees, students and visitors are responsible
for wearing the approriate PPE that will vary depending on the hazards present in the room, but must always include:

- Your **lab coat** to protect yourself as it represents an additional layer of protection for skin in case of accidental contact and splashes of hazardous substances (chemical or biological). It is also a removable barrier and it protects body’s area not completely covered by clothing.The lab coat also prevent contamination inside and outside the lab.

- Appropriate **disposable gloves** (when handling biological materials or chemicals)

<div align="center">
<img src="img/Gloves1.png" width="20%">
</div>

  Not sure which glove to use? **Consult the chemical's Safety Data Sheet (SDS) or the [safety officer](https://service.uni.lu/sp?id=sc_cat_item&sys_id=f140d745dbe83c905c72ef3c0b96194e&sysparm_category=cca7f2c1db683c905c72ef3c0b961940) for guidance.**

 It is not that trivial to safely wear and remove disposable gloves without becoming contaminated, please watch the video in [DOCEBO platform](https://unisupport.docebosaas.com/learn/course/52/play/332:103/how-to-wear-and-remove-gloves;lp=3 ).

Integrity of the gloves should be checked each time you put on new gloves. The size of the gloves has to fit to ensure good protection.


In BSL-1 and BSL-2 Labs: Only work with gloves in the laboratory
Change both gloves between different lab levels (BSL-1 or BSL-2)

<div align="center">
<img src="img/Gloves2.png" width="20%">
</div>

Do not touch the door handle or the elevator button with your gloves!


Exception is done for the doors marked by this sign:

<div align="center">
<img src="img/Gloves3.png" width="20%">
</div>
For Microscopes, computers and keyboard: stickers on the instruments indicates if gloves are required.

Important: 
Do not touch face, hair, skin with your gloves.
Do not touch your badge/phone with your gloves.



- **Specific PPEs** depending on the performed activity (faceshield, thermal gloves, themal appron,...)

It is highly recommended to also wear safety glasses or protective prescription glasses.
In addition to the lab coat, it is strongly recommended to __wear safety goggles__ to protect your eyes. You can find some in the lab coat cupboards or in the common stocks (-1 floor for BT1, -2 floor for BT2).

Other protective measures have to be taken to reduce any contamination risk such as:

- closed‐toe shoes
- clothing that covers the legs
- tie back long hair
- avoid wearing jewelry that could come into contact with biological materials/chemicals or damage the gloves
- if possible avoid to wear contact lenses, if not else possible wear eye protection over them

Follow the [Docebo Training](https://unisupport.docebosaas.com/learn/course/52/covid-19-how-to-wear-protective-equipment) to know how to wear Personal Protective Equipment.

More informations about lab coats can be found in the dedicated [How-To card]({{ '/?lab:lab-coats' | relative_url }}).

## PPE Requirements

[Classical laboratories activities](#classical-laboratories-activities)

[Work with strong chemicals](#work-with-strong-chemicals)

[Work with very strong chemicals](#work-with-very-strong-chemicals)

[Autoclave unloading and other hot thermal risks](#autoclave-unloading-and-other-hot-thermal-risks)

[Automated cryostorage](#automated-cryostorage)

[Distribution of Liquid Nitrogen in a Dewar](#distribution-of-liquid-nitrogen-in-a-dewar)

[Usage of Liquid Nitrogen in open Dewars](#usage-of-liquid-nitrogen-in-open-dewars)

[Other cold thermal risks](#other-cold-thermal-risks)

[Spill management](#spill-management)

[Work with non tested biological samples](#work-with-non-tested-biological-samples)

[Work with non-toxic smelling samples](#work-with-non-toxic-smelling-samples)

[Working with UV light](#work-with-UV's-light)

### Classical laboratories activities

For regular biomolecular, chemical, cell culture or microbiology work in BSL 1 or BSL 2 laboratories, the mandatory PPEs are

- **Lab coat**

Labcoat has to be tied up to top with sleeves fastened at the wrists, protecting the arms and torso.


- **Disposable powder free nitrile gloves**

Note: Disposable gloves must be discarded after each use or when they become contaminated.

If the gloves are not contaminated, discard them in the domestic waste bin of the lab. Otherwise, follow the [chemical and biological waste concept]({{ '/?lab:waste' | relative_url }}).

For activities with hazardous biological agents and harmful chemicals it is strongly recommended to use category III PPE gloves, and not medical exam gloves. 

Quality level and norms are often summarized with the mention Cat III and AQL (acceptance level quality) on the gloves packaging or on the technical datasheet.

<div align="center"> 
<img src="img/gloves4.png" width="50%"> 
</div> 

Disposable gloves of category III involve fulfillment of EN420, EN388, 374-2 and 374-3 requirements. Cat III is the highest protection level. 
Category III PPE gloves are tested to fulfill requirements of:

- General guidelines (identification, size, norms, composition,...) (according to norm EN420)
- Mechanical resistance (according to norm EN388)
- Penetration by chemicals (according to norm EN374-2: 2003)
- Permeation by chemicals (according to norm EN374-3: 2003)
- Protection against micro-organisms (according to norm EN374-2: 2003)

Additionally, 2 other references make sense for our specific activities: 
EN 374-5: micro-organism resistance (bacteria, fungi, viruses)
EN374-1 type B. Permeation resistance of at least 30 minutes with at least three test chemical(s). 

For activities with hazardous biological agents and harmful chemicals the use of nitrile gloves is recommended.
 #### Safety goggles (highly recommended)

<div align="center">
<img src="img/img23.png" height=207>
<img src="img/img25.png" height=207>
<img src="img/img15.png" height=207>
<img src="img/img24.png" height=207>
</div>

### Work with toxic chemicals

When handling toxic agents ex: DNA labelling reagents.

- **Lab coat**

With sleeves fastened at the wrists, protecting the arms and torso.

Note: Disposable gloves must be discarded after each use or when they become contaminated.

- **Disposable powder free long sleeves nitrile gloves**

Wear the glove above your lab coat.



- **Safety goggles** (highly recommended)

<div align="center">
<img src="img/img23.png" height="207">
<img src="img/img25.png" height="207">
<img src="img/img26.png" height="207">
<img src="img/img24.png" height="207">
</div>

### Work with very strong chemicals

Dealing with strong chemicals such as strong acids, bases,... requires specific gloves providing specific protection, on top of the regular PPEs.
Those gloves are not disposable gloves.

- **Chemical Gloves**: ANSELL: ChemTek 38-520

<div align="center">
<img src="img/img23.png" height="207">
<img src="img/img15.png" height="207">
<img src="img/img10.png" height="207">
<img src="img/img11.png" height="207">
<img src="img/img24.png" height="207">
</div>

### Autoclave unloading and other hot thermal risks

On top of the mandatory PPEs for BSL1 work, wear the **Ansell Gladiator 16-650 gloves** located next to the autoclaves to protect you against hot thermal hazard. Those gloves are not disposable gloves.

<div align="center">
<img src="img/img23.png" height="207">
<img src="img/img15.png" height="207">
<img src="img/img1.png" height="207">
<img src="img/img24.png" height="207">
</div>

### Automated cryostorage

On top of the mandatory PPEs for BSL1 work, it is mandatory to wear:

- **Cryo-gloves**



Those gloves are not disposable gloves.

- **Face shield**

<div align="center">
<img src="img/img23.png" height="207">
<img src="img/img15.png" height="207">
<img src="img/img2.png" height="207">
<img src="img/img27.png" height="207">
</div>

### Distribution of Liquid Nitrogen in a Dewar

On top of the mandatory PPEs for BSL1 work, it is mandatory to wear:

- **Cryo-gloves**


Those gloves are not disposable gloves.

- **Face shield**

- **Cryo-apron**

<div align="center">
<img src="img/img23.png" height="207">
<img src="img/img15.png" height="207">
<img src="img/img2.png" height="207">
<img src="img/img27.png" height="207">
<img src="img/img28.png" height="207">
</div>

### Usage of Liquid Nitrogen in open Dewars

Some experiments require to snap freeze the samples in Liquid Nitrogen or to keep samples frozen in Liquid Nitrogen during the manipulation. Having Liquid Nitrogen in an open Dewar on the bench top requires to wear on top of the regular PPEs:

- **Cryo-gloves**


Those gloves are not disposable gloves.

- **Face shield**

<div align="center">
<img src="img/img23.png" height="207">
<img src="img/img25.png" height="207">
<img src="img/img15.png" height="207">
<img src="img/img2.png" height="207">
<img src="img/img27.png" height="207">
</div>

### Other cold thermal risks

When handling samples in the -80°C freezers, in the -150°C freezer, in the cryopod or in dry-ice, it is recommended to wear, on top of the regular mandatory PPEs cotton white gloves to be worn under the regular nitrile glove. It will enable an improved dexterity compared to the regular cryo-gloves. Those cotton gloves are not disposable and should be kept in your lab coat.

<div align="center">
<img src="img/img23.png" height="207">
<img src="img/img25.png" height="207">
<img src="img/img6.png" height="207">
<img src="img/img15.png" height="207">
<img src="img/img24.png" height="207">
</div>

### Spill management

Spill management requires extra protection that you can find in the spill kit. On top of the regular PPEs, you have to wear:

- **Chemical and cut resistant gloves**: ANSELL: AlphaTec 58-735

To protect the skin against two type of risks: the chemicals and the glass/plasticware debris. Those gloves are disposable.

 
- **FFP2 mask with charcoal coating**

Ensure protection against particles and limited protection against chemical vapors due to the charcoal coating. A valve helps the breathing.


<div align="center">
<img src="img/img23.png" height="207">
<img src="img/img25.png" height="207">
<img src="img/img15.png" height="207">
<img src="img/img12.png" height="207">
<img src="img/img13.png" height="207">
<img src="img/img19.png" height="207">
<img src="img/img24.png" height="207">
</div>

### Work with non tested biological samples

Collective protective equipment is always the preferred option to be protected against airborne biological hazards. Therefore, biological hazards should be manipulated under biosafety cabinets. However, if after discussion with the safety officer and/or after a risk assessment made in collaboration with the safety officer lead to the impossibility to work under a biosafety cabinet, it is mandatory to wear, on top of the regular PPEs:

- **FFP2 mask with charcoal coating**

Ensure protection against particles and limited protection against chemical vapors due to the charcoal coating. A valve helps the breathing.

<div align="center">
<img src="img/img23.png" height="207">
<img src="img/img25.png" height="207">
<img src="img/img15.png" height="207">
<img src="img/img19.png" height="207">
<img src="img/img24.png" height="207">
</div>

### Work with non-toxic smelling samples

To avoid unpleasant smells in the laboratories, it is recommended to work with non-toxic samples under a chemical hood or an aspiration arm. However, if after discussion with the safety officer and/or after a risk assessment made in collaboration with the safety officer lead to the impossibility to work under a chemical hood or an aspiration arm, it is recommended to wear, on top of the regular PPEs:

- **FFP2 mask with charcoal coating**

Ensure protection against particles and limited protection against chemical vapors due to the charcoal coating. A valve helps the breathing.


<div align="center">
<img src="img/img23.png" height="207">
<img src="img/img25.png" height="207">
<img src="img/img15.png" height="207">
<img src="img/img19.png" height="207">
<img src="img/img24.png" height="207">
</div>


### Work with UV's light

**PROTECT EYES AND SKIN FROM EXPOSURE TO UV LIGHT**

- **Labcoat**

Body needs to be fully covered with long trouser, closed shoes and labcoat. Labcoat has to be tied up to top with sleeves fastened at the wrists, protecting the arms and torso. Lab workers must be particularly vigilant to prevent gaps in protective clothing that commonly occur around the neck and wrist areas.

- **Disposable powder free nitrile gloves**

Hands must be protected with long purple nitrile gloves to ensure protection of the wrist.


- **Faceshield**

As soon as UV light is switched on, face shield is mandatory (if the protective glass of the equipment can not be closed completely). All the eye protection available in the lab will not fit for this purpose. Attention need to be paid to the specification of the PPE, in accordance with the safety officer.

