---
card_order: 850
layout: page
permalink: /external/publication/phdThesisTemplate/
shortcut: publication:phdThesisTemplate
redirect_from:
  - /cards/publication:phdThesisTemplate
  - /external/cards/publication:phdThesisTemplate
---

# LaTeX template for a doctoral thesis at University of Luxembourg

A LaTeX template for PhD theses is available in GitLab at
[outreach/templates/unilu-thesis-template](https://gitlab.lcsb.uni.lu/R3/outreach/templates/thesis).

The repository includes several main items of interest:

- In the `guidelines/` directory, there is a snapshot of the official
  "Practical information for Prospective PhD candidates" PDF, which contains
  some official recommendations regarding the thesis formatting.  The snapshot
  is provided there only for convenience and its version is not necessarily
  authoritative -- ask your supervisor and committee if there is a newer
  version.

- **The thesis template itself**, present mainly in files `thesis.tex` and
  `macros.tex`. You are supposed to fill in your credentials into
  `metadata.tex`, and then continue editing the other files (`summary.tex`,
  `intro.tex`, ...) with the usual LaTeX thesis contents.

- **A simple build system for producing a PDF with the typeset thesis**. If you
  develop in GitLab or on GitHub, the thesis repository is configured so that
  it will automatically run a CI and produce a PDF artifact with the thesis
  whenever you push a new commit. To build the thesis yourself on your
  computer, you should be able to do it using any standard, sufficiently recent
  TeXlive distribution, or alternatively using Docker. The documentation in the
  repository `README.md` contains several hints on how to do that.

- The default thesis text (that you are supposed to erase and rewrite) contains
  **some minor hints about the structure of the thesis** (mainly the headings
  of the 4 main chapters), and various collected advice and demos on how to
  nicely typeset the usual material that is found in the theses, such as
  citations, figures, tables, mathematics, cross-references, appendices, etc.
  Various other things (table of contents, section numbering, ...) work
  automatically and correctly by default.

The official requirements on the thesis formatting are, apart from the
mandatory contents of the front page, committee member list and affidavit,
mostly free-form. You are allowed to customize most of the thesis look to match
the style and topic of your thesis and the requirements of your supervisor.

In case you think the thesis template should be updated, please
[open an issue in GitLab](https://gitlab.lcsb.uni.lu/R3/outreach/templates/thesis/-/issues/new)
or send a merge request. Common concerns that substantiate opening an issue
include the following:
- Because the official recommendations and requirements on the thesis form may
  change in the future, **in case you find any serious discrepancy between the
  current recommendations and the thesis template, please let the template
  maintiners know** by opening the issue, so that it can get corrected. If
  possible, attach whatever document that summarizes the new guidelines.
- If you require some specific LaTeX functionality that you think should be
  present in the template (such as advanced indexes, lists of
  floats/figures/tables, special typesetting and fonts, ...).
- If you want to share a hard-learned truth or a good approach to solve
  common problems with other students.
